# RetinoConv_WebApp
RetinoConv website for Retinopathy diabitic classification


## Installation locale

Il est facile à installer et à exécuter sur votre ordinateur.


# 1. Tout d'abord, clonez le dépôt
$ git clone https://gitlab.com/AdlesRio/RetinoConv_WebApp.git

# 2. Installez les packages Python
$ pip install -r requirements.txt

# 3. executer le script
$ python app.py


Ouvrez http: // localhost: 5000 le site se lance sur votre navigateur .
