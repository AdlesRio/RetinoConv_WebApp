import os
import sys

# Flask
from flask import Flask, redirect, url_for, request, render_template, Response, jsonify, redirect
from werkzeug.utils import secure_filename
from gevent.pywsgi import WSGIServer

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras
import efficientnet.tfkeras
from tensorflow.keras.applications.imagenet_utils import preprocess_input, decode_predictions
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image

# Some utilites
import numpy as np
from util import base64_to_pil


# Declare a flask app
app = Flask(__name__)

#load model
model = load_model('models/eff1.h5')

print('Model loaded. Check http://127.0.0.1:5000/')




#prdict function
import os
import cv2
import numpy as np
import pandas as pd
from tqdm import tqdm
SIZE=456
def model_predict(img, model):
  
    image = cv2.resize(np.float32(img), (SIZE, SIZE))
    score_predict = model.predict((image[np.newaxis])/255)
    print(score_predict)
    label_predict = np.argmax(score_predict)
    print(label_predict)
    return score_predict


@app.route('/', methods=['GET'])
def index():
    # Main page
    return render_template('index.html')


@app.route('/predict', methods=['GET', 'POST'])
def predict():
    if request.method == 'POST':
        # Get the image from post request
        img = base64_to_pil(request.json)

        # Make prediction
        score_predict = model_predict(img, model)

        # Process your result for human
        pred_proba = "{:.3f}".format(np.amax(score_predict))    # Max probability
        label_predict = np.argmax(score_predict)   # label predicted

        if label_predict == 0 :
            rst="Pas de Rétinopathie Diabétique"
        elif label_predict == 1 :
            rst="Rétinopathie Diabétique Minime"
        elif label_predict == 2 :
            rst="Rétinopathie Diabétique Modéré"
        elif label_predict == 3 :
            rst="Rétinopathie Diabétique Sévère"
        elif label_predict == 4 :
            rst="Rétinopathie Diabétique Proliférative"
        
        # Serialize the result, you can add additional fields
        return jsonify(result=rst, probability=pred_proba)

    return None


if __name__ == '__main__':
    # app.run(port=5002, threaded=False)

    # Serve the app with gevent
    http_server = WSGIServer(('0.0.0.0', 5000), app)
    http_server.serve_forever()
